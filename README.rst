.. image:: ./doc/spec-logo.png

SPEC Driver
===========
This is the simplified version of the SPEC driver which only purpose is
to export an interface to enable the users to program their bitstream on
the SPEC FPGA.

Build Sources
=============
There are no special requirements to build the SPEC.

The documentation is written in reStructuredText and it generates HTML files
and man pages. For this you need the python docutils package installed

If the requirements are satified you can run the following commant in
the project root directory:

.. code::

    make
